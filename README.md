# EcoStab

# Table of contents

- [EcoStab](#ecostab)
  * [Introduction](#introduction)
  * [Metatrancriptomics](#metatrancriptomics)
    + [Sequencing](#sequencing)
    + [Samples](#samples)
    + [Microbial communities](#microbial-communities)
    + [Genomes](#genomes)
    + [Snakemake](#snakemake)
    + [Counts normalisation](#counts-normalisation)


## Introduction
The cheese microbial community is dense with relatively low complexity and composed of cultivable prokaryotic and eukaryotic microorganisms. The ripening process can thus be reproduced under controlled conditions. A reduced microbial community composed of yeasts and bacteria was assembled to mimic that of smear-ripened cheese. The genomes of the microorganisms have been sequenced and annotated, which allows access to gene expression under various conditions at different ripening times. Through the use of a combined approach using omics (e.g. metatranscriptomics, volatilomics), and biochemical/microbial analyses, we have investigated the effect of a biotic perturbation, omission of some yeast, on the stability and functionality of the microbial cheese community throughout ripening. 

## Metatrancriptomics

Metatranscriptomics raw data were submited on ENA repostory 
[ENA](https://www.ebi.ac.uk/ena/browser/home) with the accession number PRJEB59186.  

### Sequencing 
50 cycles Truseq SBS Kit v3 (FC-401-3002, Illumina)
Instrument: HiSeq SNL176  
Insert size: 55  
Single

### Samples

| Sample_name | Biological_repeats | Time_days | Microbial community             |
|-------------|--------------------|-----------|-----------------------|
| C1          | CT7a               | T7        | Complete              |
| C2          | CT7b               | T7        | Complete              |
| C3          | CT7c               | T7        | Complete              |
| C4          | CT14a              | T14       | Complete              |
| C5          | CT14b              | T14       | Complete              |
| C6          | CT14c              | T14       | Complete              |
| C7          | CT24a              | T24       | Complete              |
| C8          | CT24b              | T24       | Complete              |
| C9          | CT24c              | T24       | Complete              |
| C10         | CT31a              | T31       | Complete              |
| C11         | CT31b              | T31       | Complete              |
| C12         | CT31c              | T31       | Complete              |
| D1          | DT7a               | T7        | *Debaryomyces hansenii* |
| D2          | DT7b               | T7        | *Debaryomyces hansenii* |
| D3          | DT7c               | T7        | *Debaryomyces hansenii* |
| D4          | DT14a              | T14       | *Debaryomyces hansenii* |
| D5          | DT14b              | T14       | *Debaryomyces hansenii* |
| D6          | DT14c              | T14       | *Debaryomyces hansenii* |
| D7          | DT24a              | T24       | *Debaryomyces hansenii* |
| D8          | DT24b              | T24       | *Debaryomyces hansenii* |
| D9          | DT24c              | T24       | *Debaryomyces hansenii* |
| D10         | DT31a              | T31       | *Debaryomyces hansenii* |
| D11         | DT31b              | T31       | *Debaryomyces hansenii* |
| D12         | DT31c              | T31       | *Debaryomyces hansenii* |
| G1          | GT7a               | T7        | *Geotrichum candidum*   |
| G2          | GT7b               | T7        | *Geotrichum candidum*   |
| G3          | GT7c               | T7        | *Geotrichum candidum*   |
| G4          | GT14a              | T14       | *Geotrichum candidum*   |
| G5          | GT14b              | T14       | *Geotrichum candidum*   |
| G6          | GT14c              | T14       | *Geotrichum candidum*   |
| G7          | GT24a              | T24       | *Geotrichum candidum*   |
| G8          | GT24b              | T24       | *Geotrichum candidum*   |
| G9          | GT24c              | T24       | *Geotrichum candidum*   |
| G10         | GT31a              | T31       | *Geotrichum candidum*   |
| G11         | GT31b              | T31       | *Geotrichum candidum*   |
| G12         | GT31c              | T31       | *Geotrichum candidum*   |


### Microbial communities

| Microbial communities |  |
|---------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Complete (C)              | *Lactococcus lactis, Kluyveromyces lactis, Debaryomyces hansenii, Geotrichum candidum, Glutamicibacter arilaitensis, Brevibacterium aurantiacum, Corynebacterium casei, Hafnia alvei, Staphylococcus equorum* |
| *Debaryomyces hansenii* (D) | *Lactococcus lactis, Kluyveromyces lactis, Debaryomyces hansenii, Glutamicibacter arilaitensis, Brevibacterium aurantiacum, Corynebacterium casei, Hafnia alvei, Staphylococcus equorum*                      |
| *Geotrichum candidum* (G)   | *Lactococcus lactis, Kluyveromyces lactis, Geotrichum candidum, Glutamicibacter arilaitensis, Brevibacterium aurantiacum, Corynebacterium casei, Hafnia alvei, Staphylococcus equorum*                        |


### Genomes

*Debaryomyces hansenii* CBS767  
Download 2019-02-09 NBCI  
[*Debaryomyces hansenii*](https://www.ncbi.nlm.nih.gov/genome/?term=txid4959)  
Files: fasta, gff3, genbank  
Download 2019-04-17 NCBI  
File: fasta protein  

*Geotrichum candidum* CLIB918  
[*Geotrichum candidum*](https://bioinformatics.psb.ugent.be/gdb/geotrichum/)  
2019-03-13  
Files: fasta, gff  

*Kluyveromyces lactis*  
Download 2019-02-09  
[*Kluyveromyces lactis*](https://www.ncbi.nlm.nih.gov/genome/?term=txid28985)    
Files: fasta, gff3, genbank  
Download 2019-04-17 NCBI  
File: fasta protein  

Bacteria  
Download 2019-03-11  
[MicroScope](https://www.genoscope.cns.fr/agc/microscope/)    
Files: fasta, gff3, genbank  
*Glutamicibacter arilaitensis* (previously *Arthrobacter arilaitensis*) Re117-chromosome NC_014550.1  
*Brevibacterium aurantiacum* BL2-WGS NZ_AAGP.1  
*Corynebacterium casei* UCMA 3821-WGS NZ_CAFW.1  
*Hafnia alvei* Ha-WGS HAAL.1  
*Lactococcus lactis* S3-WGS LALA.1  
*Staphylococcus equorum* subsp. equorum Mu2-WGS NZ_CAJL.1  
Download 2019-04-12  
COG  
Download 2019-04-17  
File: fasta CDS  
Download 2019-04-23  
File: proteins.fasta  

### Snakemake

Folder: Raw_counts

Scripts: ecostab.smk, cluster.yml, config.yml

Data:
1. Metatranscriptomics data (ENA repository: PRJEB59186)
2. Metagenome.fna
3. Metagenome.gtf
4. Meta_rRNA.fna: ribosomal RNA sequences

Module list:
1. snakemake/5.19.2
2. fastqc/0.11.9
3. cutadapt/2.10
4. singularity
5. sortmerna/4.2.0
6. star/2.7.5a
7. samtools/1.10
8. subread/1.6.1
9. slurm-drmaa/1.0.8

### Counts normalisation

Raw counts have been normalised by species/taxon with the EcoDif R library developed by Gersende Gonez, Siwar Hammami, Dominique Swennen, Marie-Agnès Dillies and Stevenn Volant (2023) according to Klingenberg and Meinicke (2017), How to normalize metatranscriptomic count data for differential expression analysis. PeerJ 5:e3859; DOI 10.7717/peerj.3859  

Folder: Data_transformation

EcoDif R library: EcoDif_1.0.0.0000.tar.gz

EcoStab metatranscriptomics data normalisation RMarkdown: EcoDif_EcoStab.Rmd

Data:
1. Raw counts: 20230926_EcoStab_RNAseq_raw_counts.tsv
2. Species file: species.tsv
3. Design: sample_classes.tsv

