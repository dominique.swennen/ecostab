FILES, = glob_wildcards(config["dataDir"]+"{files}.fastq.gz")

rule all:
    input:
        expand("FastQC/{files}_fastqc.html",files=FILES),
        expand("Cutadapt/{lane}-cutadapt.fastq.gz",lane=config["lanes"]),
        expand("FastQC-cutadapt/{lane}-cutadapt_fastqc.html",lane=config["lanes"]),
        expand("sortrna/{lane}/out/other.fastq",lane=config["lanes"]),
        "star-genome",
        expand("star-results/{lane}Aligned.sortedByCoord.out.bam",lane=config["lanes"]),
        expand("star-results/{sample}-merge.bam",sample=config["samples"]),
        expand("star-results/{sample}-merge.bam.bai",sample=config["samples"]),
        expand("star-results/{sample}_counts.txt",sample=config["samples"]),
        "star-results/counts_matrix.txt"

rule matrix_counts:
    output:
        "star-results/counts_matrix.txt"
    input:
        countfile=expand("star-results/{sample}_ftc7.txt",sample=config["samples"]),
        geneID=expand("star-results/{sample}_ftc1.txt", sample=config["samples"])
    log:
        log2="Logs/matrix_counts.log"
    shell: "cp {input.geneID[0]} star-results/ftc_geneID.txt ;\
         paste star-results/ftc_geneID.txt {input.countfile} > {output}\
         2> {log.log2}"

rule extract_counts:
    output:
        col7="star-results/{sample}_ftc7.txt",
        col1="star-results/{sample}_ftc1.txt"
    input:
        "star-results/{sample}_counts.txt"
    log:
        log2="Logs/{sample}_extract_counts.log"
    shell: "cut -f 7- {input} | sed 1d > {output.col7} ;\
         cut -f 1 {input} | sed 1d > {output.col1} ;\
         2>>{log.log2} "

rule featurecounts:
    output:
        "star-results/{sample}_counts.txt"
    input:
        "star-results/{sample}-merge.bam"
    params:
        config["dataDir"]+config["annots"]
    threads: 4
    log:
        log1="Logs/{sample}_featcounts.log1",
        log2="Logs/{sample}_featcounts.log2"
    shell: "featureCounts -t gene -a {params} -o {output} {input}\
         -T {threads} 1>>{log.log1} 2>>{log.log2}"

rule bamindex:
    output:
        "star-results/{sample}-merge.bam.bai"
    input:
        "star-results/{sample}-merge.bam"
    log:
        log1="Logs/{sample}bam_index.log1",
        log2="Logs/{sample}bam_index.log2"
    shell: "samtools index -b {input} 1>>{log.log1} 2>>{log.log2}"

rule merge:
    output:
        "star-results/{sample}-merge.bam"
    input:
        lambda wildcards:expand("star-results/{lane}Aligned.sortedByCoord.out.bam",lane=config["samples"][wildcards.sample])
    log:
        log1="Logs/{sample}bam_merge.log1",
        log2="Logs/{sample}bam_merge.log2"
    shell: "samtools merge -f {output} {input} 1>>{log.log1} 2>>{log.log2}"

rule star:
    output:
        "star-results/{lane}Aligned.sortedByCoord.out.bam"
    input:
        "sortrna/{lane}/out/other.fastq"
    params:
        genomedir = "star-genome",
        stardir = "star-results",
        prefix = "{lane}"
    threads: 48
    log:
        log2="Logs/{lane}_star.log2"
    shell: "STAR --runThreadN {threads} \
        --alignIntronMax 1500 --alignMatesGapMax 1500 --alignSJoverhangMin 25 \
        --limitBAMsortRAM 1227227947 \
        --outSAMtype BAM SortedByCoordinate --readFilesIn {input} \
        --genomeDir {params.genomedir} \
        --outFileNamePrefix {params.stardir}/{params.prefix} \
        2>>{log.log2}"

rule starGenome:
    output:
        directory("star-genome")
    input:
        genome = config["dataDir"]+config["genome"],
        annotfile = config["dataDir"]+config["annots"]
    params: exon= "ID"
    threads: 48
    log:
        log1="Logs/star-genome_index.log1",
        log2="Logs/star-genome_index.log2"
    shell: "mkdir {output} && STAR --runMode genomeGenerate --genomeDir {output} \
        --genomeFastaFiles {input.genome} --runThreadN {threads} \
        --sjdbGTFfile {input.annotfile} --genomeSAindexNbases 12 \
        --sjdbGTFtagExonParentTranscript {params.exon} \
        1>>{log.log1} 2>>{log.log2}"

rule sortmerna:
    output:
        "sortrna/{lane}/out/other.fastq"
    input:
        "Cutadapt/{lane}-cutadapt.fastq.gz"
    params:
        rna = config["dataDir"]+config["rna"],
        sortdir = lambda wildcards, output: output[0][:-16]
    threads: 96
    log:
        log2="Logs/{lane}_sortmerna.log2"
    shell: "sortmerna --ref {params.rna}\
            --reads {input} \
            --workdir {params.sortdir} \
            --num_alignments 1 --threads {threads} --fastx --other -v 2>>{log.log2}"

rule cutadaptfastqc:
    output:
        "FastQC-cutadapt/{lane}-cutadapt_fastqc.zip",
        "FastQC-cutadapt/{lane}-cutadapt_fastqc.html"
    input:
        "Cutadapt/{lane}-cutadapt.fastq.gz"
    log:
        log1="Logs/{lane}_cutadapt-fastqc.log1",
        log2="Logs/{lane}_cutadapt-fastqc.log2"
    shell: "fastqc --outdir FastQC-cutadapt/ {input} 1>>{log.log1} 2>>{log.log2}"

rule cutadapt:
    output:
        "Cutadapt/{lane}-cutadapt.fastq.gz"
    input:
        lambda wildcards: config["lanes"][wildcards.lane]
    log:
        log1="Logs/{lane}_cutadapt.log1",
        log2="Logs/{lane}_cutadapt.log2"
    shell: "cutadapt -j 0 -a TGGAATTCTCGGGTGCCAAGG \
        -m 48 -o {output} {input} \
        1>>{log.log1} 2>>{log.log2}"

rule fastqc:
    output:
        "FastQC/{files}_fastqc.zip",
        "FastQC/{files}_fastqc.html"
    input:
        "data/{files}.fastq.gz"
    log:
        log1="Logs/{files}_fastqc.log1",
        log2="Logs/{files}_fastqc.log2"
    shell: "fastqc --outdir FastQC/ {input} 1>>{log.log1} 2>>{log.log2}"

